//
//  AppDelegate.m
//  SocialNetworkVideoUploadingDemo
//
//  Created by Yaroslav Brekhunchenko on 3/17/19.
//  Copyright © 2019 Yaroslav Brekhunchenko. All rights reserved.
//

#import "AppDelegate.h"
#import "AppAuth.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    return YES;
}

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary<NSString *, id> *)options {
    if ([_currentAuthorizationFlow resumeAuthorizationFlowWithURL:url]) {
        _currentAuthorizationFlow = nil;
        return YES;
    }
    
    return NO;
}

@end
