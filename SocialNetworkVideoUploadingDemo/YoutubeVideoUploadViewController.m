//
//  YoutubeVideoUploadViewController.m
//  SocialNetworkVideoUploadingDemo
//
//  Created by Yaroslav Brekhunchenko on 3/17/19.
//  Copyright © 2019 Yaroslav Brekhunchenko. All rights reserved.
//

#import "YoutubeVideoUploadViewController.h"

#import "AppDelegate.h"
#import <CoreServices/CoreServices.h>

#import "GTLRYouTube.h"
#import <GTMAppAuth/GTMAppAuth.h>
#import <AppAuth/AppAuth.h>
#import "GTMSessionFetcher.h"
#import "GTMSessionFetcherService.h"

static NSString *const kIssuer = @"https://accounts.google.com";
static NSString *const kClientID = @"1080269235817-q3bi369g3cdrch86373eu83h19on5m8o.apps.googleusercontent.com";
static NSString *const kRedirectURI = @"com.googleusercontent.apps.1080269235817-q3bi369g3cdrch86373eu83h19on5m8o:/oauthredirect";
static NSString *const kExampleAuthorizerKey = @"authorization";

@interface YoutubeVideoUploadViewController ()

@property (nonatomic, nullable) GTMAppAuthFetcherAuthorization *authorization;
@property (nonatomic, strong) GTLRYouTubeService* youtubeService;
@property (nonatomic, strong) GTLRServiceTicket* uploadFileTicket;

@end

@implementation YoutubeVideoUploadViewController

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.youtubeService = [[GTLRYouTubeService alloc] init];
}

#pragma mark - Actions

- (IBAction)googleButtonAction:(id)sender {
    NSURL *issuer = [NSURL URLWithString:kIssuer];
    NSURL *redirectURI = [NSURL URLWithString:kRedirectURI];
    
    [OIDAuthorizationService discoverServiceConfigurationForIssuer:issuer
                                                        completion:^(OIDServiceConfiguration *_Nullable configuration, NSError *_Nullable error) {
        if (!configuration) {
            return;
        }
        
        OIDAuthorizationRequest *request = [[OIDAuthorizationRequest alloc] initWithConfiguration:configuration
                                                                                         clientId:kClientID
                                                                                           scopes:@[OIDScopeOpenID, OIDScopeProfile, kGTLRAuthScopeYouTube, kGTLRAuthScopeYouTubeUpload]
                                                                                      redirectURL:redirectURI
                                                                                     responseType:OIDResponseTypeCode
                                                                             additionalParameters:nil];
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        
        appDelegate.currentAuthorizationFlow = [OIDAuthState authStateByPresentingAuthorizationRequest:request
                                                                              presentingViewController:self
                                                                                              callback:^(OIDAuthState *_Nullable authState,
                                                                                                         NSError *_Nullable error) {
                                                                                                  if (authState) {
                                                                                                      GTMAppAuthFetcherAuthorization *authorization = [[GTMAppAuthFetcherAuthorization alloc] initWithAuthState:authState];
                                                                                                      self.authorization = authorization;
                                                                                                      
                                                                                                      self.youtubeService.authorizer = self.authorization;
                                                                                                      
                                                                                                      [self saveState];
                                                                                                      [self uploadVideoFile];
                                                                                                  } else {
                                                                                                      NSLog(@"Error. Unable to authorize to google.");
                                                                                                  }
                                                                                              }];
    }];
}

#pragma mark - Utilz

- (void)uploadVideoFile {
    GTLRYouTube_VideoStatus *status = [GTLRYouTube_VideoStatus object];
    status.privacyStatus = @"public";
    
    GTLRYouTube_VideoSnippet *snippet = [GTLRYouTube_VideoSnippet object];
    snippet.title = @"title";
    NSString *desc = @"description";
    if (desc.length > 0) {
        snippet.descriptionProperty = desc;
    }
    NSString *tagsStr = @"tags";
    if (tagsStr.length > 0) {
        snippet.tags = [tagsStr componentsSeparatedByString:@","];
    }
    
    GTLRYouTube_Video *video = [GTLRYouTube_Video object];
    video.status = status;
    video.snippet = snippet;
    
    [self uploadVideoWithVideoObject:video
             resumeUploadLocationURL:nil];
}

- (void)uploadVideoWithVideoObject:(GTLRYouTube_Video *)video
           resumeUploadLocationURL:(NSURL *)locationURL {
    NSURL *fileToUploadURL = [[NSBundle mainBundle] URLForResource:@"booking_demo" withExtension:@"mov"];
    NSError *fileError;
    NSLog(@"step");
    if (![fileToUploadURL checkPromisedItemIsReachableAndReturnError:&fileError]) {
        
        NSLog(@"exit");
        
        return;
    }
    
    NSString *filename = [fileToUploadURL lastPathComponent];
    NSString *mimeType = [self MIMETypeForFilename:filename
                                   defaultMIMEType:@"video/mp4"];
    GTLRUploadParameters *uploadParameters =
    [GTLRUploadParameters uploadParametersWithFileURL:fileToUploadURL
                                             MIMEType:mimeType];
    uploadParameters.uploadLocationURL = locationURL;
    
    GTLRYouTubeQuery_VideosInsert *query = [GTLRYouTubeQuery_VideosInsert queryWithObject:video
                                                                                     part:@"snippet,status"
                                                                         uploadParameters:uploadParameters];
    
    query.executionParameters.uploadProgressBlock = ^(GTLRServiceTicket *ticket,
                                                      unsigned long long numberOfBytesRead,
                                                      unsigned long long dataLength) {
        NSLog(@"upload progress");
        
    };
    
    self.uploadFileTicket = [self.youtubeService executeQuery:query
                                            completionHandler:^(GTLRServiceTicket *callbackTicket,
                                                                GTLRYouTube_Video *uploadedVideo,
                                                                NSError *callbackError) {
                                                if (callbackError == nil) {
                                                    NSLog(@"uploaded");
                                                } else {
                                                    NSLog(@"error %@",callbackError);
                                                }
                                            }];
}

- (NSString *)MIMETypeForFilename:(NSString *)filename
                  defaultMIMEType:(NSString *)defaultType {
    NSString *result = defaultType;
    NSString *extension = [filename pathExtension];
    CFStringRef uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,
                                                            (__bridge CFStringRef)extension, NULL);
    if (uti) {
        CFStringRef cfMIMEType = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType);
        if (cfMIMEType) {
            result = CFBridgingRelease(cfMIMEType);
        }
        CFRelease(uti);
    }
    return result;
}

- (void)saveState {
    if (_authorization.canAuthorize) {
        [GTMAppAuthFetcherAuthorization saveAuthorization:_authorization
                                        toKeychainForName:kExampleAuthorizerKey];
    } else {
        [GTMAppAuthFetcherAuthorization removeAuthorizationFromKeychainForName:kExampleAuthorizerKey];
    }
}

@end
