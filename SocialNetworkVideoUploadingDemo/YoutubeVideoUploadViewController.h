//
//  YoutubeVideoUploadViewController.h
//  SocialNetworkVideoUploadingDemo
//
//  Created by Yaroslav Brekhunchenko on 3/17/19.
//  Copyright © 2019 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YoutubeVideoUploadViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
