//
//  AppDelegate.h
//  SocialNetworkVideoUploadingDemo
//
//  Created by Yaroslav Brekhunchenko on 3/17/19.
//  Copyright © 2019 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OIDAuthorizationFlowSession;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(nonatomic, strong, nullable) id<OIDAuthorizationFlowSession> currentAuthorizationFlow;


@end

