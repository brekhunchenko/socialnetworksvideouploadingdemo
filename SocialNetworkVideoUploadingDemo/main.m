//
//  main.m
//  SocialNetworkVideoUploadingDemo
//
//  Created by Yaroslav Brekhunchenko on 3/17/19.
//  Copyright © 2019 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
